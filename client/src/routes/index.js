import { lazy } from "react";

const CreateUser = lazy(() => import("../components/CreateUser"));
const SearchUser = lazy(() => import("../components/SearchUser"));
const EditUser = lazy(() => import("../components/EditUser"));

const routes = [
  {
    path: "/",
    component: <CreateUser />,
    exact: true,
  },
  {
    path: "/search",
    component: <SearchUser />,
    exact: false,
  },
  {
    path: "/edit",
    component: <EditUser />,
    exact: false,
  },
  // {
  //   path: "/counter",
  //   component: <Counter />,
  //   exact: false,
  // },
  // {
  //   path: "/counter-class",
  //   component: <CounterClass />,
  //   exact: false,
  // },
  // {
  //   path: "/rating",
  //   component: <RatingComponent />,
  //   exact: false,
  // },
  // {
  //   path: "/rating-class",
  //   // component: <RatingClassComponent />,
  //   component: <Wrapper />,
  //   exact: false,
  // },
  {
    path: "*",
    component: "Not Found",
    exact: false,
  },
];

export default routes;
