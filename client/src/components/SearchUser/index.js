import { React, createRef, Component } from "react";
import formImg from "../../logo.png";
import "./search.css";

class SearchUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      experience: "",
      level: "",
      submitData: {
        username: "",
        email: "",
        experience: "",
        level: "",
      },
    };
    this.formWrapperElement = createRef(null);
  }

  handleUsername = (data) => {
    this.setState({
      username: data.target.value,
    });
  };

  handleEmail = (data) => {
    this.setState({
      email: data.target.value,
    });
  };

  handleExperience = (data) => {
    this.setState({
      experience: data.target.value,
    });
  };

  handleLevel = (data) => {
    this.setState({
      level: data.target.value,
    });
  };

  handleSubmit = () => {
    const { level, experience, username, email } = this.state;
    this.setState({
      submitData: {
        username: username,
        email: email,
        experience: experience,
        level: level,
      },
    });
  };

  render() {
    return (
      <div id="app-search" ref={this.formWrapperElement}>
        <div className="right-side">
          <h3>Search User</h3>
        </div>
        <div className="search">
          <div className="container">
            <div className="base-container" ref={this.props.containerRef}>
              <div className="content">
                <div className="image">
                  <img src={formImg} />
                </div>
                <div className="form">
                  <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input
                      onChange={this.handleUsername}
                      value={this.state.username}
                      type="text"
                      name="username"
                      placeholder="username"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input
                      onChange={this.handleEmail}
                      value={this.state.email}
                      type="email"
                      name="email"
                      placeholder="email"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="experience">Experience</label>
                    <input
                      onChange={this.handleExperience}
                      value={this.state.experience}
                      name="experience"
                      placeholder="experience"
                      type="text"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="level">Level</label>
                    <input
                      onChange={this.handleLevel}
                      value={this.state.level}
                      name="level"
                      placeholder="level"
                      type="text"
                    />
                  </div>
                </div>
              </div>
              <div className="footer">
                <button
                  onClick={this.handleSubmit}
                  type="button"
                  className="btn"
                >
                  Submit
                </button>
              </div>
              <div className="input-data">
                <div>{`Username: ${this.state.submitData.username}`}</div>
                <div>{`Email: ${this.state.submitData.email}`}</div>
                <div>{`Experience: ${this.state.submitData.experience}`}</div>
                <div>{`Level: ${this.state.submitData.level}`}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchUser;
