import { React, createRef, Component } from "react";
import formImg from "../../logo.png";
import "./edit.css";

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      submitData: {
        username: "",
        email: "",
      },
    };
    this.formWrapperElement = createRef(null);
  }

  handleUsername = (data) => {
    this.setState({
      username: data.target.value,
    });
  };

  handleEmail = (data) => {
    this.setState({
      email: data.target.value,
    });
  };

  handleSubmit = () => {
    const { username, email } = this.state;
    this.setState({
      submitData: {
        username: username,
        email: email,
      },
    });
  };

  render() {
    return (
      <div id="app-edit" ref={this.formWrapperElement}>
        <div className="right-side">
          <h3>Edit User</h3>
        </div>
        <div className="search">
          <div className="container">
            <div className="base-container" ref={this.props.containerRef}>
              <div className="content">
                <div className="image">
                  <img src={formImg} />
                </div>
                <div className="form">
                  <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input
                      onChange={this.handleUsername}
                      value={this.state.username}
                      type="text"
                      name="username"
                      placeholder="username"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input
                      onChange={this.handleEmail}
                      value={this.state.email}
                      type="email"
                      name="email"
                      placeholder="email"
                    />
                  </div>
                </div>
              </div>
              <div className="footer">
                <button
                  onClick={this.handleSubmit}
                  type="button"
                  className="btn"
                >
                  Save
                </button>
              </div>
              <div className="input-data">
                <div>{`Username: ${this.state.submitData.username}`}</div>
                <div>{`Email: ${this.state.submitData.email}`}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditUser;
