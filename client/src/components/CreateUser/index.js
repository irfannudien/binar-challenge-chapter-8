import { React, createRef, Component } from "react";
import formImg from "../../logo.png";
import "./create.css";
import { Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import StickyHeadTable from "./table";

class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmType: "password",
      username: "",
      email: "",
      password: "",
      confirmPassword: "",
      submitData: {
        username: "",
        email: "",
        password: "",
        confirm: "",
      },
    };
    this.formWrapperElement = createRef(null);
  }

  handleUsername = (data) => {
    this.setState({
      username: data.target.value,
    });
  };

  handleEmail = (data) => {
    this.setState({
      email: data.target.value,
    });
  };

  handlePassword = (data) => {
    this.setState({
      password: data.target.value,
    });
  };

  handleConfirmPassword = (data) => {
    this.setState({
      confirmPassword: data.target.value,
    });
  };

  handleSubmit = () => {
    const { username, email, password, confirmPassword } = this.state;
    this.setState({
      submitData: {
        username: username,
        email: email,
        password: password,
        confirm: confirmPassword,
      },
    });
  };

  render() {
    console.log(this.state);
    return (
      <div id="app-create" ref={this.formWrapperElement}>
        <div className="right-side">
          <h3>Create User</h3>
        </div>
        <div className="search">
          <div className="container">
            <div className="base-container" ref={this.props.containerRef}>
              <div className="content">
                <div className="image">
                  <img src={formImg} />
                </div>
                <div className="form">
                  <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input
                      onChange={this.handleUsername}
                      value={this.state.username}
                      type="text"
                      name="username"
                      placeholder="username"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input
                      onChange={this.handleEmail}
                      value={this.state.email}
                      type="email"
                      name="email"
                      placeholder="email"
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                      onChange={this.handlePassword}
                      value={this.state.password}
                      name="password"
                      placeholder="password"
                      type={this.state.confirmType}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="confirm-password">Confirm Password</label>
                    <input
                      onChange={this.handleConfirmPassword}
                      value={this.state.confirmPassword}
                      name="confirm-password"
                      placeholder="confirm"
                      type={this.state.confirmType}
                    />
                  </div>
                  <FormGroup className="checkbox">
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.confirmType === "text"}
                          onChange={() => {
                            if (this.state.confirmType === "text") {
                              this.setState({ confirmType: "password" });
                            } else {
                              this.setState({ confirmType: "text" });
                            }
                          }}
                        />
                      }
                      label="Show Password"
                    />
                  </FormGroup>
                </div>
              </div>
              <div className="footer">
                <button
                  onClick={this.handleSubmit}
                  type="button"
                  className="btn"
                >
                  Submit
                </button>
              </div>
              <div className="input-data">
                <div>{`Username: ${this.state.submitData.username}`}</div>
                <div>{`Email: ${this.state.submitData.email}`}</div>
                <div>{`Password: ${this.state.submitData.password}`}</div>
                <div>{`Confirm Password: ${this.state.submitData.confirm}`}</div>
              </div>
              <div className="table-stick">
                <StickyHeadTable rows={this.state.submitData} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateUser;
